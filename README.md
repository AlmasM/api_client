# API-ZeebeClient

API and zeebeClient written in Scala using akka http \
The deliveryHandle2 is our zeebeClient implementation \
The httpTutorial is our API implementation


## Usage
* start the zeebe broker
* deploy `delivery-process` workflow
* start API server (it will start at localhost 8080)
* start zeebe cleint (it will start at localhost 8081)

## Details
`delivery-process` workflow looks like this:\
![delivery process](diagram.png) \
in order to startwork flow you have to first call api with `POST` `localhost:8080/order/startWorkflow` and embed payload: 
```json
{
  "address":"abaya 66",
  "details":"burger and doner",
  "userId":"12345d"
}
```
you may assume that `userId` always exist, so you can write any random string. \
After that api calls zeebe client to start new instance of workflow. \
I took scenario of delivery service where depending on delivery optimization coefficient service will initiate delivery. However, if coefficient is bigger than 3 the delivery will be rejected and deleted from repository. You can get more insight from `UML activity` diagram and `bpmn`.

