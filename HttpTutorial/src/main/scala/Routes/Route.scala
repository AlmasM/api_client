package Routes

import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.server
import akka.util.Timeout


trait Route {
  implicit val system: ActorSystem[_]
  implicit val timeout: Timeout = Timeout.create(system.settings.config.getDuration("http-duration"))
  val actor: ActorRef[_]
  val route: server.Route
}