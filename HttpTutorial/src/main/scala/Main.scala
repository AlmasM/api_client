import Actors.OrderActor
import Routes.OrderRoute
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.server
import com.typesafe.config.{Config, ConfigFactory}

object Main {
  def startHttpServer(route: server.Route, system: ActorSystem[_], config: Config): Unit = {
    new HttpServer(route, system, config).start()
  }
  def main(args: Array[String]): Unit = {
    val rootBehavior = Behaviors.setup[Nothing] {
      context =>
        implicit val system = context.system
        val config = ConfigFactory.load()
        val orderActor = context.spawn(OrderActor(config), "Actors.OrderActor")
        system.log.info("server is starting")

        val orderRoute = new OrderRoute(orderActor).orderRoute
        startHttpServer(orderRoute, system, config)
        Behaviors.empty
    }
    ActorSystem[Nothing](rootBehavior, "main")
  }
}