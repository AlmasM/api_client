package Actors

import Actors.OrderActor._
import Domains.{Order, StartWorkflowRequest}
import Repositories.OrderRepository
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import com.typesafe.config.Config
import io.circe.generic.auto._
import io.circe.syntax._

import scala.util.{Failure, Random, Success}

object OrderActor{
  sealed trait Command
  final case class CreateOrder(
                               address:String,
                               details: String,
                               userId: String,
                               replyTo: ActorRef[Either[ErrorInfo, Order]]
                              ) extends Command
  final case class OptimizeOrderDistance(
                                     orderId: String,
                                     replyTo: ActorRef[Either[ErrorInfo, Order]]
                                   ) extends Command
  final case class RemoveOrder(
                                orderId: String,
                                replyTo: ActorRef[Either[ErrorInfo, Order]]
                              ) extends Command
  final case class GetOrder(
                             orderId: String,
                             replyTo: ActorRef[Either[ErrorInfo, Order]]
                           ) extends Command
  final case class StartWorkflow(
                                  address:String,
                                  details:String,
                                  userId:String,
                                  replyTo: ActorRef[Either[ErrorInfo, HttpResponse]]
                                ) extends Command

  def apply(config: Config): Behavior[Command] = {
    Behaviors.setup(context => new OrderActor(context, config).receive())
  }
}

class OrderActor(context: ActorContext[Command], config: Config){
  implicit val system = context.system
  implicit val executionContext = system.executionContext

  def sendRequest(address:String, details:String, userId: String) = {
    system.log
        .info("sending request to start work flow")
    Http().singleRequest(HttpRequest(
        method = HttpMethods.POST,
        uri = Uri(config.getString("zeebeClient.startWorkflowUri")),
        entity = HttpEntity(
          ContentTypes.`application/json`,
          StartWorkflowRequest(address, details, userId).asJson.noSpaces
        )
      )
    )
  }

  def generateOptimizedDistance(orderId: String): Float = {
    val distance = Random.between(0f, 4f) // assume here some interesting algorithm
    system.log
        .info(s"generated optimized distance $distance for order with id=$orderId")
    distance
  }

  def receive(): Behavior[Command] = {
    Behaviors.receiveMessage{
      case createOrder: CreateOrder =>
        createOrder.replyTo ! Right(
          OrderRepository.createOrder(
            createOrder.address,
            createOrder.details,
            createOrder.userId
          )
        )
        Behaviors.same
      case optimizeOrderDistance: OptimizeOrderDistance =>
        OrderRepository.setDistance(
          orderId = optimizeOrderDistance.orderId,
          distance = generateOptimizedDistance(optimizeOrderDistance.orderId)
        ) match {
          case Some(updated) =>
            optimizeOrderDistance.replyTo ! Right(
              updated
            )
          case _ =>
            optimizeOrderDistance.replyTo ! Left(
              new ErrorInfo(summary = "Such order does not exists")
            )
        }
        Behaviors.same
      case removeOrder: RemoveOrder =>
        OrderRepository.removeOrder(orderId = removeOrder.orderId) match {
          case Some(removed) =>
            removeOrder.replyTo ! Right(
              removed
            )
          case _ =>
            removeOrder.replyTo ! Left(
              new ErrorInfo(summary = "Such order does not exists")
            )
        }
        Behaviors.same
      case getOrder: GetOrder =>
        OrderRepository.getOrder(
          orderId = getOrder.orderId,
        ) match {
          case Some(found) =>
            getOrder.replyTo ! Right(
              found
            )
          case _ =>
            getOrder.replyTo ! Left(
              new ErrorInfo(summary = "Such order does not exists")
            )
        }
        Behaviors.same
      case startWorkflow: StartWorkflow =>
        sendRequest(startWorkflow.address, startWorkflow.details, startWorkflow.userId) onComplete {
          case Success(response) =>
            startWorkflow.replyTo ! Right(response)
          case Failure(_) =>
            startWorkflow.replyTo ! Left(
              new ErrorInfo(summary = "Something bad happened")
            )
        }
        Behaviors.same
    }
  }
}
