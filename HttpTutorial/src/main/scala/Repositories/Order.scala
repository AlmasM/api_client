package Repositories
import java.util.UUID.randomUUID

import Domains.Order

object OrderRepository{
  var orders: List[Order] = List.empty

  def createOrder(address: String, details: String, userId: String): Order = {
    val createdOrder = Order(
      id = randomUUID().toString,
      address = address,
      details = details,
      userId = userId,
      None
    )
    orders = createdOrder :: orders
    createdOrder
  }

  def setDistance(orderId: String, distance: Float): Option[Order] = {
    removeOrder(orderId) match {
      case Some(removed) =>
        val updated = Order(
          removed.id,
          removed.address,
          removed.details,
          removed.userId,
          Some(distance)
        )
        orders = updated :: orders
        Some(updated)
      case _ =>
          None
    }
  }

  def removeOrder(orderId: String): Option[Order] = {
    orders.find(order => orderId == order.id) match {
        case Some(found) =>
          orders = orders.filter(order => found == order) // remove element
          Some(found)
        case _ =>
          None
      }
  }

  def getOrder(orderId: String): Option[Order] = {
    orders.find(order => orderId == order.id)
  }
}