name := "HttpTutorial"

version := "0.1"

scalaVersion := "2.13.3"

val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-slf4j"       % AkkaVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
  "ch.megard" %% "akka-http-cors" % "1.1.0",
  "io.circe"          %% "circe-core"           % "0.13.0",
  "io.circe"          %% "circe-generic"        % "0.13.0",
  "io.circe"          %% "circe-parser"         % "0.13.0",
  "io.circe"          %% "circe-literal"        % "0.13.0",
  "io.circe"          %% "circe-generic-extras" % "0.13.0",
  "io.circe" %% "circe-jawn" % "0.13.0",
  "joda-time" % "joda-time"    % "2.10.5",
  "org.joda"  % "joda-convert" % "2.2.1",
  "io.circe" %% "circe-bson" % "0.4.0",
  "de.heikoseeberger" %% "akka-http-circe" % "1.31.0",
)