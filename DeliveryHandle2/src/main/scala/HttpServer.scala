import akka.Done
import akka.actor.CoordinatedShutdown
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.RouteConcatenation
import akka.http.scaladsl.{Http, server}
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class HttpServer(route: server.Route, system: ActorSystem[_], config: Config) extends RouteConcatenation{
  implicit val classicActorSystem: akka.actor.ActorSystem = system.classicSystem
  implicit val executionContext: ExecutionContext = system.executionContext
  private val shutdown = CoordinatedShutdown(system)
  def start(): Unit = {
    Http().newServerAt(config.getString("app.host"), config.getInt("app.port")).bind(route).onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system.log
        .info(s"host: ${address.getHostName}, port ${address.getPort}")
        shutdown.addTask(CoordinatedShutdown.PhaseServiceRequestsDone, "http-graceful-terminate") { () =>
          binding.terminate(10.seconds).map { _ =>
            system.log
              .info("DAR-api http://{}:{}/ graceful shutdown completed", address.getHostString, address.getPort)
            Done
          }
        }
      case Failure(_) =>
        system.terminate()
    }
  }
}