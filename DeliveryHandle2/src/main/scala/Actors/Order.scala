package Actors


import Actors.Order.{Command, CreateOrder, RejectOrder}
import akka.Done
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import com.typesafe.config.Config
import io.zeebe.client.api.response.ActivatedJob

import scala.concurrent.Await
import scala.concurrent.duration._
object Order{
  abstract class Command
  final case class CreateOrder(job: ActivatedJob, replyTo: ActorRef[HttpResponse]) extends Command
  final case class RejectOrder(job: ActivatedJob, replyTo: ActorRef[Done]) extends Command
  def apply(config: Config): Behavior[Command] =
      Behaviors.setup(context => new Order(context,config))

}

class Order (context: ActorContext[Command], config: Config)
  extends AbstractBehavior[Command](context) {
  implicit val system = context.system
  implicit val executionContext = system.executionContext

  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case CreateOrder(job, replyTo) =>
        val orderDetails =  job.getVariables // already json
        val responseFuture = Http().singleRequest(HttpRequest(
            method = HttpMethods.POST,
            uri = Uri(config.getString("api.orderUri")),
            entity = HttpEntity(ContentTypes.`application/json`, orderDetails)
          )
        )
        responseFuture.map(
          response =>
            replyTo ! response
        )
        try {
          Await.result(responseFuture, 5.seconds)
        }catch{
          case e: Throwable => system.log
            .info(s"something went wrong, nothing returned after 5 seconds ${e.getMessage}")
        }
        Behaviors.stopped
      case RejectOrder(job, replyTo) =>
        val orderId =  job
          .getVariablesAsMap
          .get("id").
          asInstanceOf[String]

        val responseFuture = Http().singleRequest(HttpRequest(
          method = HttpMethods.DELETE,
          uri = Uri(config.getString("api.orderUri"))
            .withQuery(Query("orderId" -> orderId))
          )
        )
        responseFuture.map(
          _ =>
            replyTo ! Done
        )
        try {
          Await.result(responseFuture, 5.seconds)
        }catch{
          case e: Throwable => system.log
            .info(s"something went wrong, nothing returned after 5 seconds ${e.getMessage}")
        }
        Behaviors.stopped
      case _ =>
        system.log
            .info("Unexpected message had come")
        Behaviors.stopped
    }
  }
}
