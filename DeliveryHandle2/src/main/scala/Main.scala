import Routes.Route
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.server
import com.typesafe.config.{Config, ConfigFactory}
import io.zeebe.client.ZeebeClient



object Main extends App {
  def startHttpServer(route: server.Route, system: ActorSystem[_], config: Config) = {
    new HttpServer(route = route, system, config).start()
  }
  def startZeebeClient(implicit actorSystem: ActorSystem[_], config: Config): ZeebeClient ={
    val zeebeClient: ZeebeClient = ZeebeClient.newClientBuilder().brokerContactPoint("localhost:26500")
      .usePlaintext().build()
    zeebeClient.newWorker().jobType("create-order").handler{
      new ServiceWorker(zeebeClient, config)
    }.open()
    zeebeClient.newWorker().jobType("generate-optimal").handler{
      new ServiceWorker(zeebeClient, config)
    }.open()
    zeebeClient.newWorker().jobType("reject-delivery").handler{
      new ServiceWorker(zeebeClient, config)
    }.open()
    zeebeClient.newWorker().jobType("init-free").handler{
      new ServiceWorker(zeebeClient, config)
    }.open()
    zeebeClient.newWorker().jobType("init-standart").handler{
      new ServiceWorker(zeebeClient, config)
    }.open()
    zeebeClient.newWorker().jobType("init-express").handler{
      new ServiceWorker(zeebeClient, config)
    }.open()
    zeebeClient
  }

  val rootBehavior = Behaviors.setup[Nothing] {
    context =>
      implicit val system = context.system
      val config = ConfigFactory.load()
      system.log
      .info("Server is starting")
      val zeebeClient = startZeebeClient(system, config)
      val route = new Route(zeebeClient).route
      startHttpServer(route, system, config)
      Behaviors.empty
  }
  ActorSystem[Nothing](rootBehavior, "main")
}