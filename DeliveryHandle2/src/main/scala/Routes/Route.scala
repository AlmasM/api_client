package Routes

import Actors.Client
import Actors.Client.StartWorkflow
import Routes.Route.StartWorkflowRequest
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server
import akka.http.scaladsl.server.Directives._
import io.zeebe.client.ZeebeClient
import spray.json.DefaultJsonProtocol._

object Route{
  case class StartWorkflowRequest(address: String, details: String, userId: String)
  implicit val requestBodyFormat = jsonFormat3(StartWorkflowRequest)
}

class Route(val zeebeClient: ZeebeClient){
  val route: server.Route =
    concat(
      post{
        path("start-workflow"){
          entity(as[StartWorkflowRequest]) { body =>
            ActorSystem(Client(zeebeClient), "clientActor") ! StartWorkflow(body.address, body.details, body.userId)
            complete(StatusCodes.Accepted)
          }
        }
      }
    )
}
